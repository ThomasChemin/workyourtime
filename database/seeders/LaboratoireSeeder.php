<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use \App\Models\Laboratoire;

class LaboratoireSeeder extends Seeder {

    /**
     * Run the database seeds.
     * Insert into table 'laboratoire' some data
     * @return void
     */
    public function run() {
        Laboratoire::factory()
                ->times(5)
                ->create();
    }

}
