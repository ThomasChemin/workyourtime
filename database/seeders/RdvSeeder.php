<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Rdv;

class RdvSeeder extends Seeder {

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run() {
        Rdv::factory()
                ->times(150)
                ->create();
    }

}
