<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\User;

class UtilisateurSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        for ($i = 0; $i < 50; $i++) {
            User::factory()
                    ->create();
        }
    }
}
