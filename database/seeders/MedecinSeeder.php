<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Medecin;

class MedecinSeeder extends Seeder {

    /**
     * Run the database seeds.
     * Run the MedecinFactory
     * @return void
     */
    public function run() {
        for ($i = 0; $i < 50; $i++) {
            Medecin::factory()
                    ->create();
        }
    }

}
