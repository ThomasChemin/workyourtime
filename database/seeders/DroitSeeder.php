<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Droit;

class DroitSeeder extends Seeder {

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run() {
        $droits = [
            ['id' => 1, 'name' => 'Visiteur'],
            ['id' => 2, 'name' => 'Secretaire'],
            ['id' => 3, 'name' => 'Admin'],
        ];
        foreach ($droits as $droit){
            $d = new Droit($droit);
            $d->save();
        }
    }

}
