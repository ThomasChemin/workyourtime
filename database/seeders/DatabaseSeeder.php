<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;

class DatabaseSeeder extends Seeder {

    /**
     * Seed the application's database.
     * Run all the factories
     * @return void
     */
    public function run() {
        $addAdmin = new \App\Models\User(['id' => 1, 'name' => 'Administrator', 'email' => 'admin@gmail.com', 'password' => \Illuminate\Support\Facades\Hash::make('Admin$69'), 'email_verified_at' => now(), 'remember_token' => Str::random(10)]);
        $addAdmin->save(); //Add an Administrator in users
        $this->call([
            LaboratoireSeeder::class,
            DroitSeeder::class,
            MedecinSeeder::class,
            UtilisateurSeeder::class,
            UtilisateurDroitSeeder::class,
        ]);
        $addAdminRights = new \App\Models\UtilisateurDroit(['idUser' => 1, 'idDroit' => 3]);
        $addAdminRights->save(); //Add the Rights to the Administrator in UserDroit
        $this->call([
            RdvSeeder::class,
        ]);
    }

}
