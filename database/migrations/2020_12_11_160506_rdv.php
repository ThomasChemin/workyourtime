<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Rdv extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('Rdv', function (Blueprint $table) {
            $table->date('jour');
            $table->integer('heures');
            $table->text('motif');
            $table->integer('idUser');
            $table->integer('idMedecin');
            $table->primary(['jour', 'heures', 'idUser']);
            $table->foreign('idUser')->references('id')->on('users');
            $table->foreign('idMedecin')->references('id')->on('Medecin');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('Rdv');
    }
}
