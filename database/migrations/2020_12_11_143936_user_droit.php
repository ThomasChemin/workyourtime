<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UserDroit extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('UserDroit', function (Blueprint $table) {
            $table->integer('idUser');
            $table->integer('idDroit');
            $table->primary(['idUser', 'idDroit']);
            $table->timestamps();
            $table->foreign('idDroit')->references('id')->on('Droit');
            $table->foreign('idUser')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('UserDroit');
    }
}
