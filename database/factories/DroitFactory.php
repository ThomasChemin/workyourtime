<?php

namespace Database\Factories;

use App\Models\Droit;
use Illuminate\Database\Eloquent\Factories\Factory;

class DroitFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Droit::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            
        ];
    }
}
