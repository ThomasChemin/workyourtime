<?php

namespace Database\Factories;

use App\Models\Rdv;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Facades\DB;

class RdvFactory extends Factory {

    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Rdv::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition() {
        $laboratoireId = DB::table('Laboratoire')
                        ->inRandomOrder()
                        ->first()
                ->id;
        $idMed = (DB::table('Medecin')->where('Laboratoire', '=', $laboratoireId)
                        ->inRandomOrder()
                        ->first())
                ->id;
        $idUser = (DB::table('users')
                        ->inRandomOrder()
                        ->first())
                ->id;
        $date = $this->faker->dateTimeThisDecade->format('Y-m-d');
        if ($this->faker->numberBetween(1, 2) == 1) {
            $heure = $this->faker->numberBetween(8, 11); //Hour between 8 and 11
        } else {
            $heure = $this->faker->numberBetween(13, 17); //Hour between 13 and 17
        }
        $motif = $this->faker->text;
        return [
            "jour" => $date,
            "heures" => $heure,
            "motif" => $motif,
            "idMedecin" => $idMed,
            "idUser" => $idUser,
        ];
    }

}
