<?php

namespace Database\Factories;

use App\Models\User;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;

class UserFactory extends Factory {

    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = User::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition() {
        $this->faker->addProvider(new \Faker\Provider\fr_FR\Address($this->faker));
        $this->faker->addProvider(new \Faker\Provider\fr_FR\Person($this->faker));
        $this->faker->addProvider(new \Faker\Provider\fr_FR\PhoneNumber($this->faker));

        $firstName = $this->faker->firstName;
        if ($firstName[0] == 'É' || $firstName[0] == 'È') { //Prevent an error when insert in database because of a 'É'
            $firstName[0] = 'e';
        }

        return [
            'name' => $this->faker->lastName,
            'prenom' => $firstName,
            'adresse' => $this->faker->address,
            'age' => $this->faker->numberBetween($min = 26, $max = 70),
            'ville' => $this->faker->city,
            'email' => $this->faker->unique()->safeEmail,
            'email_verified_at' => now(),
            'password' => $password = \Illuminate\Support\Facades\Hash::make($this->faker->word . $this->faker->randomNumber($nbDigits = 5, $strict = true)),
            'telephone' => $this->faker->unique()->phoneNumber,
            'remember_token' => Str::random(10),
        ];
    }

}
