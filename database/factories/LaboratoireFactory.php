<?php

namespace Database\Factories;

use App\Models\Laboratoire;
use Illuminate\Database\Eloquent\Factories\Factory;

class LaboratoireFactory extends Factory {

    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Laboratoire::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition() {

        $this->faker->addProvider(new \Faker\Provider\fr_FR\Address($this->faker));
        
        return [
            'region' => $this->faker->region,
            'adresse' => $this->faker->address,
        ];
    }

}
