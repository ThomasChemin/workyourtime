<?php

namespace Database\Factories;

use App\Models\Medecin;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;

class MedecinFactory extends Factory {

    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Medecin::class;

    /**
     * Define the model's default state.
     * Add a User in table users
     * Add a new medecin in Medecin with random properties
     * Add the Rights to a Medecin
     * @return array
     */
    public function definition() {

        $this->faker->addProvider(new \Faker\Provider\fr_FR\Address($this->faker));
        $this->faker->addProvider(new \Faker\Provider\fr_FR\Person($this->faker));
        $this->faker->addProvider(new \Faker\Provider\fr_FR\PhoneNumber($this->faker));

        $nom = $this->faker->lastName;
        $prenom = $this->faker->firstName;
        if ($prenom[0] == 'É' || $prenom[0] == 'È') { //Prevent an error when insert in database because of a 'É'
            $prenom[0] = 'e';
        }
        $mail = strtolower($prenom . '.' . $nom) . '@' . $this->faker->freeEmailDomain;
        $laboratoire = (DB::table('Laboratoire')->inRandomOrder()->first())->id;
        $medecinRemplacant = DB::table('Medecin')->where('Laboratoire', '=', $laboratoire)->inRandomOrder()->first();
        if ($medecinRemplacant == null) {
            $codeRemplacant = 1;
        } else {
            $codeRemplacant = $medecinRemplacant->id;
        }
        return [
            'numLicence' => strval($this->faker->randomNumber($nbDigits = 5, $strict = true)),
            'name' => $nom,
            'prenom' => $prenom,
            'adresse' => $this->faker->address,
            'age' => $this->faker->numberBetween($min = 26, $max = 70),
            'ville' => $this->faker->city,
            'email' => $mail,
            'telephone' => $this->faker->unique()->phoneNumber,
            'laboratoire' => $laboratoire,
            'remplacant' => $codeRemplacant,
        ];
    }

}
