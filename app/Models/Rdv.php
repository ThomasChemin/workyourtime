<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Rdv extends Model {

    use HasFactory;

    /**
     * The table the model refer to
     * @var type 
     */
    protected $table = 'Rdv';
    /**
     * The primary key of the table
     * @var type 
     */
    protected $primaryKey = ['jour', 'heures', 'idUser', 'idMedecin'];
    /**
     * Attributes that are not able be fillable
     * @var type 
     */
    protected $guarded = [];
    /**
     * Set the autoIncrement. Depend on the attribute
     * @var type 
     */
    public $incrementing = false;
    /**
     * Set the timeStamps in the DB. Depend on the attribute
     * @var type 
     */
    public $timestamps = true;
    /**
     * Set the type of the primary key if different from an integer
     * @var type 
     */
    protected $keyType = 'string';

    /**
     * Get the primary Key from his name
     * @return type
     */
    public function getKey() {
        $attributes = [];
        foreach ($this->getKeyName() as $key) {
            $attributes[$key] = $this->getAttribute($key);
        }

        return $attributes;
    }
    
    /**
     * Relation between Rdv and Medecin
     * @return type
     */
    public function medecin(){
        return $this->hasOne(Medecin::class, 'id', 'idMedecin');
    }
    
    /**
     * Relation between Rdv and Visiteur
     * @return type
     */
    public function user(){
        return $this->hasOne(User::class, 'id', 'idUser');
    }

}
