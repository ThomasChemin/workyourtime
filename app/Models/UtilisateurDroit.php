<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class UtilisateurDroit extends Model {

    use HasFactory;

    /**
     * The table the model refer to
     * @var type 
     */
    protected $table = 'UserDroit';

    /**
     * The primary key of the table
     * @var type 
     */
    protected $primaryKey = ['idUser', 'idDroit'];

    /**
     * Attributes that are not able be fillable
     * @var type 
     */
    protected $guarded = [];

    /**
     * Set the autoIncrement. Depend on the attribute
     * @var type 
     */
    public $incrementing = false;

    /**
     * Set the timeStamps in the DB. Depend on the attribute
     * @var type 
     */
    public $timestamps = true;

    /**
     * Get the primary Key from his name
     * @return type
     */
    public function getKey() {
        $attributes = [];
        foreach ($this->getKeyName() as $key) {
            $attributes[$key] = $this->getAttribute($key);
        }

        return $attributes;
    }

    /**
     * Relation between UserDroit and User
     * @return type
     */
    public function user() {
        return $this->hasOne(User::class, 'id', 'idUser');
    }

    /**
     * Relation between UserDroit and Droit
     * @return type
     */
    public function droit() {
        return $this->hasOne(Droit::class, 'id', 'idDroit');
    }

}
