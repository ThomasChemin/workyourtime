<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Laboratoire extends Model
{
    use HasFactory;
    
    /**
     * The table the model refer to
     * @var type 
     */
    protected $table = 'Laboratoire';
    /**
     * The primary key of the table
     * @var type 
     */
    protected $primaryKey = "id";
    /**
     * Attributes that are not able be fillable
     * @var type 
     */
    protected $guarded = [];
    /**
     * Set the autoIncrement. Depend on the attribute
     * @var type 
     */
    public $incrementing = false;
    /**
     * Set the timeStamps in the DB. Depend on the attribute
     * @var type 
     */
    public $timestamps = true;
    /**
     * Get the Medecins for the Laboratoire.
     */
    public function medecin()
    {
        return $this->hasMany(Medecin::class, 'laboratoire');
    }
}
