<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Medecin extends Model
{
    use HasFactory;
    
    /**
     * The table the model refer to
     * @var type 
     */
    protected $table = 'Medecin';
    /**
     * The primary key of the table
     * @var type 
     */
    protected $primaryKey = "id";
    /**
     * Attributes that are not able be fillable
     * @var type 
     */
    protected $guarded = [];
    /**
     * Set the autoIncrement
     * @var type 
     */
    /**
     * Set the autoIncrement. Depend on the attribute
     * @var type 
     */
    public $incrementing = false;
    /**
     * Set the timeStamps in the DB. Depend on the attribute
     * @var type 
     */
    public $timestamps = true;
    /**
     * Get the Laboratoire for the Medecin.
     */
    public function laboratory()
    {
        return $this->hasOne(Laboratoire::class, 'id', 'laboratoire');
    }
    
    /**
     * Relation between Medecin and Rdv
     * @return type
     */
    public function rdv(){
        return $this->hasMany(Rdv::class, 'idMedecin');
    }
}
