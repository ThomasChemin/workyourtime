<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Facades\Auth;
use Illuminate\Routing\Redirector;
use Acaronlex\LaravelCalendar\Event;
use Acaronlex\LaravelCalendar\Calendar;
use App\Models\Rdv;
use \Illuminate\Support\Facades\Validator;
use App\Models\Medecin;

class RdvController extends Controller {

    /**
     * Display the page 'confirmeRdv'
     * @param type $medecinId the id of the medecin
     * @param type $eventDate the date of the rdv
     * @param type $eventHeure the hour of the rdv
     * @return to view confirmationRdv
     */
    public function confirmRdv($medecinId, $eventDate, $eventHeure) {
        $title = 'Confirmation du rendez-vous';
        $medecin = Medecin::find($medecinId);
        $labo = $medecin->laboratory;
        return view('confirmationRdv', ['title' => $title,
            'medecin' => $medecin,
            'date' => $eventDate,
            'heure' => $eventHeure,
            'labo' => $labo]);
    }

    /**
     * Insert in the database a new Rdv
     * @param Request->$request
     * @return back with errors if the information are not correct |
     * to the route calendarVisiteur
     */
    public function validateRdv(Request $request) {
        $validData = Validator::make($request->all(), [
                    'heure' => 'required',
                    'date' => 'required',
                    'medecin' => 'required',
                    'motif' => ['required', 'min:3'],
                        ], $this->messages());
        if ($validData->fails()) {
            return redirect()->back()->withInput()->withErrors($validData);
        }
        try {
            Rdv::create(['jour' => $request->date,
                'heures' => intval($request->heure),
                'idUser' => Auth::id(),
                'idMedecin' => intval($request->medecin),
                'motif' => $request->motif]);
        } catch (\Exception $ex) {
            return back()->withErrors(["date" => "Vous avez déjà un rendez-vous à cette date et à cette heure."]);
        }
        return redirect()->route('calendarVisiteur', ['id' => Auth::id()]);
    }

    /**
     * Display the page 'modifRdv'
     * @param Request->$request
     * @return to view modifRdv
     */
    public function modifRdv(Request $request) {
        foreach (Auth::user()->userDroit as $userDroit) {
            if ($userDroit->idDroit == 1 || $userDroit->idDroit == 3) {
                $rdv = Rdv::where('jour', '=', $request->date)
                        ->where('heures', '=', $request->heure)
                        ->where('idUser', '=', Auth::id())
                        ->where('idMedecin', '=', $request->medecin)
                        ->first();
                break;
            } else if ($userDroit->idDroit == 2) {
                $rdv = Rdv::where('jour', '=', $request->date)
                        ->where('heures', '=', $request->heure)
                        ->where('idMedecin', '=', $request->medecin)
                        ->first();
                break;
            }
        }
        if($rdv == null){
            return redirect()->back();
        }
        $title = 'Modification de rendez-vous';
        return view('modifRdv', ['title' => $title, 'rdv' => $rdv]);
    }

    /**
     * Do the modification on the current Rdv
     * @param Request->$request
     * @return to view modifRdv if an error occur | 
     * to calendarVisiteur if it is a success and the user is a visiteur
     * to showDispoOfMedecinForSec if it success and the user is a secretaire
     */
    public function doModifRdv(Request $request) {
        $title = 'Modification de rendez-vous';
        $validData = Validator::make($request->all(), [
                    'heure' => ['required', 'numeric', 'between:8,17'],
                    'date' => ['required', 'date', 'after_or_equal:today'],
                    'medecin' => 'required',
                    'motif' => ['required', 'min:3'],
                        ], $this->messages());
        $lastRdv = Rdv::where('jour', '=', $request->last_date)
                ->where('heures', '=', $request->last_heure)
                ->where('idMedecin', '=', $request->last_medecin)
                ->first();
        if ($validData->fails() || $request->heure == 12 || $request->heure == 13 || (new \DateTime($request->date))->format('D') == 'Sat' || (new \DateTime($request->date))->format('D') == 'Sun') {
            if ($request->heure == 12 || $request->heure == 13) {
                $validData->errors()->add('heure', 'L\'heure ne doit pas être comprise entre 12h et 13h');
            }
            if ((new \DateTime($request->date))->format('D') == 'Sat' || (new \DateTime($request->date))->format('D') == 'Sun') {
                $validData->errors()->add('date', 'Vous ne pouvez pas déplacer un rendez-vous à un Samedi ou un Dimanche.');
            }
            return view('modifRdv', ['title' => $title,
                                'rdv' => $lastRdv])
                            ->withErrors($validData);
        }
        try {
            DB::table('Rdv')
                    ->where('jour', '=', $request->last_date)
                    ->where('heures', '=', $request->last_heure)
                    ->where('idMedecin', '=', $request->last_medecin)
                    ->delete();
            DB::table('Rdv')
                    ->insert([
                        'jour' => $request->date,
                        'heures' => $request->heure,
                        'motif' => $request->motif,
                        'idUser' => $lastRdv->idUser,
                        'idMedecin' => $request->medecin,
            ]);
            foreach (Auth::user()->userDroit as $userDroit) {
                if ($userDroit->idDroit == 1) {
                    return redirect()->route('calendarVisiteur', ['id' => Auth::id()]);
                } else if ($userDroit->idDroit == 2) {
                    return redirect()->route('showDispoOfMedecinForSec', ['id' => $request->medecin]);
                }
            }
        } catch (\Exception $ex) {
            DB::table('Rdv')
                    ->insert([
                        'jour' => $lastRdv->jour,
                        'heures' => $lastRdv->heures,
                        'motif' => $lastRdv->motif,
                        'idUser' => $lastRdv->idUser,
                        'idMedecin' => $lastRdv->idMedecin,
            ]);
            return view('modifRdv', ['title' => $title, 'rdv' => $lastRdv])->withErrors(["Erreur" => "Une erreur est survenue lors de la modification du rendez-vous. Veuillez vérifier les horaires du médecin."]);
        }
    }

    /**
     * Delete a Rdv that the User has selected
     * @param Request->$request
     * @return to calendarVisiteur if the user is a visiteur | 
     * to showDispoOfMedecinForSec if the user is a secretaire |
     * to accueil
     */
    public function suppRdv(Request $request) {
        Rdv::where('jour', '=', $request->date)
                ->where('heures', '=', $request->heure)
                ->where('idMedecin', '=', $request->medecin)
                ->delete();
        foreach (Auth::user()->userDroit as $userDroit) {
            if ($userDroit->idDroit == 1) {
                return redirect()->route('calendarVisiteur', ['id' => Auth::id()]);
            } else if ($userDroit->idDroit == 2) {
                return redirect()->route('showDispoOfMedecinForSec', ['id' => $request->medecin]);
            }
        }
        return redirect()->route('accueil');
    }

    /**
     * Delete all the Rdv already passed from the current visitor
     * @return to view caldendarVisiteur
     */
    public function suppAllRdvPass() {
        DB::table('Rdv')
                ->select()
                ->where('jour', '<', NOW()->format('Y-m-d'))
                ->where('idUser', '=', Auth::id())
                ->delete();
        DB::table('Rdv')
                ->select()
                ->where('jour', '=', NOW()->format('Y-m-d'))
                ->where('heures', '<', intval(NOW()->format('H')) + 1)
                ->where('idUser', '=', Auth::id())
                ->delete();
        return redirect()->route('calendarVisiteur', ['id' => Auth::id()]);
    }

    /**
     * Display all the 'rdv' of a visiteur
     * @param type $visiteurId
     * @return to view calendar
     */
    public function showVisiteurRdv($visiteurId) {
        $trueId = Auth::id();
        if ($visiteurId != $trueId) {
            return redirect()->route('calendarVisiteur', ['id' => $trueId]);
        }
        $title = "Mes rendez-vous";
        $events = [];
        $rdvs = DB::table('Rdv')->select()->where('idUser', '=', $visiteurId)->get();
        foreach ($rdvs as $rdv) {
            $date = new \DateTime($rdv->jour);
            $date->setTime($rdv->heures - 2, 0);
            $events[] = \Calendar::event(
                            $rdv->motif, //event title
                            false, //full day event?
                            $date, //start time, must be a DateTime object or valid DateTime format (http://bit.ly/1z7QWbg)
                            $date->setTime($rdv->heures - 1, 0), //end time, must be a DateTime object or valid DateTime format (http://bit.ly/1z7QWbg),
                            1, //optional event ID
                            [
                                'url' => URL::route('gererRdv', ['visiteurId' => Auth::id(),
                                    'jour' => $rdv->jour,
                                    'heures' => $rdv->heures])
                            ]
            );
        }
        $calendar = new Calendar();
        $calendar->addEvents($events)
                ->setOptions([
                    'locale' => 'fr',
                    'firstDay' => 0,
                    'displayEventTime' => true,
                    'selectable' => true,
                    'initialView' => 'timeGridWeek',
                    'headerToolbar' => [
                        'end' => 'today prev,next dayGridMonth timeGridWeek timeGridDay'
                    ]
        ]);
        $calendar->setId('1');
        $calendar->setCallbacks([
            'select' => 'function(selectionInfo){}',
            'eventClick' => 'function(event){}'
        ]);
        return view('calendar', ['title' => $title,
            'calendar' => $calendar, 'personalCalendar' => true]);
    }

    /**
     * Display the page 'gererRdv'
     * @param type $visiteurId the id of the Visiteur
     * @param type $jour the day of the Rdv
     * @param type $heure the hour of the Rdv
     * @return view gererRdv
     */
    public function gererVisiteurRdv($visiteurId, $jour, $heure) {
        $title = 'Gérer mes rendez-vous';
        $rdv = Rdv::where('idUser', '=', $visiteurId)
                ->where('jour', '=', $jour)
                ->where('heures', '=', $heure)
                ->first();
        $medecin = $rdv->medecin;
        $laboratoire = $medecin->laboratory;
        return view('gererRdv', ['title' => $title,
            'rdv' => $rdv,
            'medecin' => $medecin,
            'laboratoire' => $laboratoire]);
    }

    public function gererRdvBySec($medId, $jour, $heure) {
        $rdv = Rdv::where('idMedecin', '=', $medId)
                ->where('jour', '=', $jour)
                ->where('heures', '=', $heure)
                ->first();
        $medecin = $rdv->medecin;
        $labo = $medecin->laboratory;
        $title = "Gérer les rendez-vous de " . $medecin->name . ' ' . $medecin->prenom;
        return view('gererRdv', ['title' => $title,
            'rdv' => $rdv,
            'medecin' => $medecin,
            'laboratoire' => $labo]);
    }

    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages() {
        return [
            'motif.required' => 'Il faut remplir le motif.',
            'motif.min' => 'Le motif doit faire plus de 3 lettres.',
            'heure.required' => 'Une heure doit être renseignée.',
            'heure.between' => 'L\'heure doit être comprise entre 8h-11h et 14h-17h.',
            'date.required' => 'Une date doit être renseignée.',
            'date.after_or_equal' => 'Le rendez-vous ne peut pas être déplacé à une date déjà passée.',
            'medecin.required' => 'Un medecin doit être renseigné.',
        ];
    }

}
