<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use \Illuminate\Support\Facades\Validator;
use \App\Models\Laboratoire;

class LaboratoireController extends Controller {

    /**
     * Display all the Laboratories from a region
     * @param type $region the name of the region of laboratories
     * @return to view rendezVous
     */
    public function showLabFromRegion($region) {
        $title = 'Prendre rendez-vous';
        $labos = DB::table('Laboratoire')->select()->where('region', 'LIKE', "%$region%")->Paginate(10);
        return view('rendezVous', ['title' => $title, 'labos' => $labos]);
    }

    /**
     * Display the page addLaboratory
     * @return view addLaboratory
     */
    public function addLaboratory() {
        return view('addLaboratory');
    }

    /**
     * 
     * @param Request $request
     * @return back with input and errors if information are not correct | to a new view addLaboratory if it is a success
     */
    public function doAddLaboratory(Request $request) {
        $validData = Validator::make($request->all(), [
                    'adresse' => 'required',
                    'region' => 'required',
                        ], $this->messages());
        if ($validData->fails()) {
            return redirect()->back()->withInput()->withErrors($validData);
        }
        $laboratory = new Laboratoire([
            "region" => $request->get('region'),
            "adresse" => $request->get('adresse'),
        ]);
        try {
            $laboratory->save();
        } catch (Exception $ex) {
            return redirect()->back()->withInput()->withErrors(['errorInsert' => 'Une erreur est survenue lors de l\'ajout du laboratoire.']);
        }
        return redirect()->route("addLaboratory")->withInput(['successInsert' => 'Le laboratoire a bien été ajouté.']);
    }
    
    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages() {
        return [
            'required' => 'Le champ :attribute doit être rempli.',
        ];
    }

}
