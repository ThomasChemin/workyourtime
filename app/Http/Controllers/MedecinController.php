<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\DB;
use Acaronlex\LaravelCalendar\Event;
use Illuminate\Support\Facades\URL;
use Acaronlex\LaravelCalendar\Calendar;
use App\Models\Medecin;
use Illuminate\Http\Request;

class MedecinController extends Controller {

    /**
     * Display all the Medecins that are part of a Laboratoire
     * @param type $labId the id of the laboratory
     * @return to view showMedecin
     */
    public function showMedFromLaboId($labId = null) {
        if ($labId == null) {
            return redirect()->back();
        }
        $title = "Prendre un rendez-vous";
        $medecins = DB::table('Medecin')->select()->where('laboratoire', '=', $labId)->Paginate(10);
        return view('showMedecin', ['title' => $title, 'medecins' => $medecins]);
    }

    /**
     * Display the calendar of a Medecin to show the disponibilities
     * @param type $medecinId the id of the medecin
     * @return to view calendar
     */
    public function showDispoOfMedecinId($medecinId = null) {
        if ($medecinId == null) {
            return redirect()->back();
        }
        $title = "Choisir un horaire";
        $events = [];
        $rdvs = DB::table('Rdv')->select()->where('idMedecin', '=', $medecinId)->get();
        if ($rdvs != null) {
            foreach ($rdvs as $rdv) {
                $date = new \DateTime($rdv->jour);
                $date->setTime($rdv->heures - 1, 0);
                $endHeure = $date->format('H') + 1;
                $events[] = \Calendar::event(
                                'Indisponible', //event title
                                false, //full day event?
                                $date, //start time, must be a DateTime object or valid DateTime format (http://bit.ly/1z7QWbg)
                                new \DateTime(date($date->format('Y') . $date->format('m') . ($date->format('d')) . " $endHeure:0:0")), //end time, must be a DateTime object or valid DateTime format (http://bit.ly/1z7QWbg),
                                1, //optional event ID
                                [
                                    'url' => '',
                                    'backgroundColor' => 'red',
                                ]
                );
            }
        }
        $today = new \DateTime(date("Y-m-d H:i:sP"));
        $today->setTime($today->format('H'), 0);
        for ($jour = 0; $jour < 7; $jour++) {
            if ($today->format('D') == 'Sat' || $today->format('D') == 'Sun') {
                
            } else {
                for ($heure = $today->format('H') + 1; $heure < 17; $heure++) {
                    if ($heure == 11 || $heure == 12) {
                        
                    } else {
                        $willBeCreated = true;
                        foreach ($events as $event) {
                            if ($event->start->format('Y-m-d H:m:sP') == (new \DateTime(date($today->format('Y') . $today->format('m') . ($today->format('d')) . " $heure:0:0")))->format('Y-m-d H:m:sP')) {
                                $willBeCreated = false;
                            }
                        }
                        if ($willBeCreated) {
                            $endHeure = $heure + 1;
                            $events[] = \Calendar::event(
                                            'Libre', //event title
                                            false, //full day event?
                                            new \DateTime(date($today->format('Y') . $today->format('m') . ($today->format('d')) . " $heure:0:0")), //start time, must be a DateTime object or valid DateTime format (http://bit.ly/1z7QWbg)
                                            new \DateTime(date($today->format('Y') . $today->format('m') . ($today->format('d')) . " $endHeure:0:0")), //end time, must be a DateTime object or valid DateTime format (http://bit.ly/1z7QWbg),
                                            count($rdvs) + $jour + $heure, //optional event ID
                                            [
                                                'url' => URL::route("confirmationRdv", ['medecinId' => $medecinId, 'eventDate' => (new \DateTime(date($today->format('Y') . $today->format('m') . ($today->format('d')) . " $heure:0:0")))->format('Y-m-d'), 'eventHeure' => (new \DateTime(date($today->format('Y') . $today->format('m') . ($today->format('d')) . " $endHeure:0:0")))->format('H')]),
                                            ]
                            );
                        }
                    }
                }
            }
            if ($today->format('m') % 2 == 0 && $today->format('d') <= 29) {
                $today = new \DateTime(date("Y-". $today->format('m') ."-" . ($today->format('d') + 1) . " 6:0:0"));
            } 
            else if($today->format('m') % 2 == 1 && $today->format('d') <= 30){
                $today = new \DateTime(date("Y-". $today->format('m') ."-" . ($today->format('d') + 1) . " 6:0:0"));
            }
            else {
                $today = new \DateTime(date("Y-" . ($today->format('m') + 1) . "-1 6:0:0"));
            }
        }
        $calendar = new Calendar();
        $calendar->addEvents($events)
                ->setOptions([
                    'locale' => 'fr',
                    'firstDay' => 0,
                    'displayEventTime' => true,
                    'selectable' => true,
                    'initialView' => 'timeGridWeek',
                    'headerToolbar' => [
                        'end' => 'today prev,next dayGridMonth timeGridWeek timeGridDay'
                    ]
        ]);
        $calendar->setId('1');
        $calendar->setCallbacks([
            'select' => 'function(selectionInfo){}',
            'eventClick' => 'function(event){}'
        ]);
        return view('calendar', ['title' => $title, 'calendar' => $calendar, 'personalCalendar' => false]);
    }

    /**
     * Display the calendar of a Medecin to show the disponibilities
     * @param type $medecinId the id of the medecin
     * @return to view calendar
     */
    public function showDispoOfMedecinIdSec($medecinId = null) {
        if ($medecinId == null) {
            return redirect()->back();
        }
        $title = "Choisir un horaire";
        $events = [];
        $rdvs = DB::table('Rdv')->select()->where('idMedecin', '=', $medecinId)->get();
        if ($rdvs != null) {
            foreach ($rdvs as $rdv) {
                $date = new \DateTime($rdv->jour);
                $date->setTime($rdv->heures - 1, 0);
                $endHeure = $date->format('H') + 1;
                $events[] = \Calendar::event(
                                $rdv->motif, //event title
                                false, //full day event?
                                $date, //start time, must be a DateTime object or valid DateTime format (http://bit.ly/1z7QWbg)
                                new \DateTime(date($date->format('Y') . $date->format('m') . ($date->format('d')) . " $endHeure:0:0")), //end time, must be a DateTime object or valid DateTime format (http://bit.ly/1z7QWbg),
                                1, //optional event ID
                                [
                                    'url' => URL::route('gererRdvBySec', ['medId' => $medecinId, 'jour' => $rdv->jour, 'heures' => $rdv->heures]),
                                ]
                );
            }
        }
        $calendar = new Calendar();
        $calendar->addEvents($events)
                ->setOptions([
                    'locale' => 'fr',
                    'firstDay' => 0,
                    'displayEventTime' => true,
                    'selectable' => true,
                    'initialView' => 'timeGridWeek',
                    'headerToolbar' => [
                        'end' => 'today prev,next dayGridMonth timeGridWeek timeGridDay'
                    ]
        ]);
        $calendar->setId('1');
        $calendar->setCallbacks([
            'select' => 'function(selectionInfo){}',
            'eventClick' => 'function(event){}'
        ]);
        $medecin = Medecin::find($medecinId);
        return view('calendar', ['title' => $title, 'calendar' => $calendar, 'personalCalendar' => false, 'medecin' => $medecin]);
    }

}
