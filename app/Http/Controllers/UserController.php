<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use App\Models\User;
use App\Models\Visiteur;
use App\Models\UtilisateurDroit;
use Illuminate\Support\Str;
use \Illuminate\Support\Facades\Validator;
use App\Models\Medecin;

class UserController extends Controller {

    /**
     * Logout the current User
     * @return back with input
     */
    public function deconnexion() {
        Auth::logout();
        $title = 'Accueil';
        return back()->withInput([
                    'confirmation' => 'Vous avez bien été déconnecté.'
        ]);
    }

    /**
     * Display the 'Inscription' page
     * @return to view inscription
     */
    public function inscription() {
        $title = 'Inscription';
        return view('inscription', ['title' => $title]);
    }

    /**
     * Insert in the DB a 'Visiteur' with the rights 'CRU'
     * @param Request $request
     * @return back with input if information are not correct | to view login
     */
    public function doInscription(Request $request) {
        $validData = Validator::make($request->all(), [
                    'password' => ['required', 'same:passwordValidation', 'regex:/(?=.*\d)(?=.*[!?=#$]+)(?=.*[A-Z])(?=.*[a-z]).*$/'],
                    'passwordValidation' => ['required'],
                    'email' => ['required', 'regex:/^[a-zA-Z0-9]+([._+-][a-zA-Z0-9]+)*@[a-zA-Z]{2,}[.][a-zA-Z]{2,3}?$/', 'unique:users,email,'],
                    'name' => ['required', 'regex:/^[A-Z][a-z]+/'],
                    'prenom' => ['required', 'regex:/^[A-Z][a-z]+/'],
                    'adresse' => ['required'],
                    'telephone' => ['required', 'regex:/[0-9]{10}/', 'numeric', 'unique:users,telephone'],
                    'ville' => ['required', 'regex:/[A-Z]{1}[a-z]+/'],
                    'age' => ['required', 'regex:/[0-9]+/', 'between:25,70', 'numeric'],
                    'role' => ['required'],
                        ], $this->messages());
        if ($validData->fails()) {
            return redirect()->back()->withInput()->withErrors($validData);
        }
        $password = \Illuminate\Support\Facades\Hash::make($request->get('password'));
        $newUser = User::create([
            'name' => $request->get('name'),
            'prenom' => $request->get('prenom'),
            'adresse' => $request->get('adresse'),
            'age' => $request->get('age'),
            'ville' => $request->get('ville'),
            'email' => $request->get('email'),
            'email_verified_at' => now(),
            'password' => $password,
            'telephone' => $request->get('telephone'),
            'remember_token' => Str::random(10),
        ]);
        $newUserRights = UtilisateurDroit::create([
            'idUser' => $newUser->id,
            'idDroit' => $request->get('role'),
            'created_at' => now(),
            'updated_at' => now(),
        ]);
        $title = 'Connexion';
        return view('auth/login', ['title' => $title]);
    }

    /**
     * Display the page 'Profil'
     * @return to view profil
     */
    public function profil() {
        $title = 'Profil';
        $user = User::find(Auth::id());
        $nbRdvTomorrow = DB::table('Rdv')
                ->select()
                ->where('idUser', '=', Auth::id())
                ->where('jour', '>', now()->format('Y-m-d'))
                ->count();
        $nbRdvToday = DB::table('Rdv')
                ->select()
                ->where('idUser', '=', Auth::id())
                ->where('jour', '=', now()->format('Y-m-d'))
                ->where('heures', '>', intval(now()->format('H')))
                ->count();
        $nbRdv = $nbRdvToday + $nbRdvTomorrow;
        return view('profil', ['title' => $title, 'user' => $user, 'nbRdv' => $nbRdv]);
    }

    /**
     * Attribute a Medecin to a User that have the Droit 'secretaire'
     * @param type $medecinId te id of the medecin
     * @return to route showDispoOfMedecinForSec
     */
    public function attributeMedtoSec($medecinId) {
        $sec = User::find(Auth::id());
        $sec->idMedecinRef = $medecinId;
        $sec->save();
        return redirect()->route('showDispoOfMedecinForSec', ['id' => $medecinId]);
    }

    /**
     * Get the error messages for the defined validation rules.
     * @return type
     */
    public function messages() {
        return [
            'password.same' => 'Votre mot de passe ne correspond pas au mot de passe de confirmation.',
            'password.regex' => 'Votre mot de passe doit faire au moins 6 caractères et doit contenir une majuscule, une minuscule, un chiffre et un symbole parmit ["!" "?" "=" "#" "$"].',
            'email.regex' => 'Votre email ne correspond pas aux normes.',
            'email.unique' => 'L\'email saisi a déjà été pris.',
            'name.regex' => 'Votre nom doit contenir une majuscule au début.',
            'prenom.regex' => 'Votre prenom doit contenir une majuscule au début.',
            'telephone.regex' => 'Votre numéro de téléphone ne correspond pas aux normes.',
            'telephone.unique' => 'Le numéro de téléphone saisi a déjà été pris.',
            'telephone.numeric' => 'Le téléphone doit être composé de chiffre.',
            'ville.regex' => 'Votre ville doit contenir une majuscule au début.',
            'age.regex' => 'Votre âge doit être fait de chiffres.',
            'age.between' => 'Votre âge doit être compris entre 25 et 70ans.',
            'age.numeric' => 'L\'âge doit être composé de chiffre.',
            'required' => 'Le champ :attribute doit être rempli.',
        ];
    }

}
