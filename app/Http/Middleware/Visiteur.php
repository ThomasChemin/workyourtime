<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class Visiteur {

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next) {
        $roles = Auth::user()->userDroit;
        foreach ($roles as $role) {
            if ($role->idDroit == 1 || $role->idDroit == 3) {
                return $next($request);
            }
        }
        return redirect()->back()->withErrors(['noRights' => 'Vous n\'avez pas les droits d\'accéder à cette page']);
    }

}
