<?php

use Illuminate\Support\Facades\Route;
use \App\Http\Controllers\UserController;
use \App\Http\Controllers\LaboratoireController;
use \App\Http\Controllers\MedecinController;
use \App\Http\Controllers\CalendarController;
use \App\Http\Controllers\VisiteurController;
use \App\Http\Controllers\RdvController;
use \App\Http\Controllers\Auth\LoginController;
use \App\Http\Controllers\Auth\RegisterController;

/*
  |--------------------------------------------------------------------------
  | Web Routes
  |--------------------------------------------------------------------------
  |
  | Here is where you can register web routes for your application. These
  | routes are loaded by the RouteServiceProvider within a group which
  | contains the "web" middleware group. Now create something great!
  |
 */

/**
 * Routes that refers to the Home
 */
Route::group(['prefix' => ''], function() {
    Route::get('/', function () {
        return view('accueil', ['title' => 'Accueil']);
    })->name('accueil');
    Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
});

/**
 * Routes that refers to the User
 */
Route::group(['prefix' => 'utilisateur'], function() {
    Route::get('/connexion', [LoginController::class, 'showLoginForm'])->name('connexion');
    Route::post('/doConnexion', [LoginController::class, 'login'])->name('doConnexion');
    Route::get('/inscription', [UserController::class, 'inscription'])->name('inscription');
    Route::post('/doInscription', [UserController::class, 'doInscription'])->name('doInscription');
    Route::get('/deconnexion', [LoginController::class, 'logout'])->middleware('auth')->name('deconnexion');
    Route::get('/profil', [UserController::class, 'profil'])->middleware('auth')->name('profil');
});

/**
 * Routes for the admin
 */
Route::group(['prefix' => 'admin'], function() {
    Route::get('/addLaboratory', [LaboratoireController::class, 'addLaboratory'])->middleware(['auth','admin'])->name('addLaboratory');
    Route::post('/doAddLaboratory', [LaboratoireController::class, 'doAddLaboratory'])->middleware(['auth','admin'])->name('doAddLaboratory');
});

/**
 * Routes that refers to a Rdv
 */
Route::group(['prefix' => 'rdv'], function() {
    Route::get('/gererRdv/{visiteurId}/{jour}/{heures}', [RdvController::class, 'gererVisiteurRdv'])->middleware('auth')->name('gererRdv');
    Route::get('', function() {
        return view('region', ['title' => 'Prendre un rendez vous']);
    })->middleware('auth')->name('region');
    Route::get('/region/{name}', [LaboratoireController::class, 'showLabFromRegion'])->middleware('auth')->name('rdv');
    Route::get('/showMedFromLabo/{id?}', [MedecinController::class, 'showMedFromLaboId'])->middleware('auth')->name('showMedFromLabo');
    Route::post('/modifRdv/{date}/{heure}', [RdvController::class, 'modifRdv'])->middleware('auth')->name('modifRdv');
    Route::post('/suppRdv/{date}/{heure}', [RdvController::class, 'suppRdv'])->middleware('auth')->name('suppRdv');
    Route::post('/doModifRdv', [RdvController::class, 'doModifRdv'])->middleware('auth')->name('doModifRdv');
    Route::get('/modifRdv/{date}/{heure}', [RdvController::class, 'modifRdv'])->middleware('auth')->name('modifRdv');
    Route::get('/suppAllRdvPass', [RdvController::class, 'suppAllRdvPass'])->middleware('auth')->name('suppAllRdvPass');
});

/**
 * Routes that refers to a Rdv for a Visiteur
 */
Route::group(['prefix' => 'rdvVis'], function() {
    Route::get('/confirmation/{medecinId}/{eventDate}/{eventHeure}', [RdvController::class, 'confirmRdv'])->middleware(['auth','visiteur'])->name('confirmationRdv');
    Route::post('/validate', [RdvController::class, 'validateRdv'])->middleware(['auth','visiteur'])->name('validateRdv');
    Route::get('/showDispoOfMedecin/{id?}', [MedecinController::class, 'showDispoOfMedecinId'])->middleware(['auth','visiteur'])->name('showDispoOfMedecin');
});

/**
 * Routes that refers to a Rdv for a Secretaire
 */
Route::group(['prefix' => 'rdvSec'], function() {
    Route::get('/gererRdvMed/{medId}/{jour}/{heures}', [RdvController::class, 'gererRdvBySec'])->middleware(['auth','secretaire'])->name('gererRdvBySec');
    Route::get('/showDispoOfMedecinSec/{id?}', [MedecinController::class, 'showDispoOfMedecinIdSec'])->middleware(['auth','secretaire'])->name('showDispoOfMedecinForSec');
    Route::get('/attributeMedtoSec/{idMed}', [UserController::class, 'attributeMedtoSec'])->middleware(['auth','secretaire'])->name('attributeMedtoSec');
});

/**
 * Routes that refers to a calendar
 */
Route::get('/calendarVis/{id}', [RdvController::class, 'showVisiteurRdv'])->middleware(['auth','visiteur'])->name('calendarVisiteur');

Auth::routes();