@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Inscription') }}</div>
                @foreach($errors->all() as $error)
                <p style="color: red">{{ $error }}</p>
                @endforeach
                <div class="card-body">
                    <form method="POST" action="{{URL::route('doInscription')}}">
                        @csrf
                        <div class="input-group mb-2 mr-sm-2">
                            <div class="input-group-prepend">
                                <div class="input-group-text">Email</div>
                            </div>
                            <input type="mail" 
                                   class="form-control"
                                   name="email"
                                   value="{{old('email')}}"
                                   placeholder="Email. Exemple : j.dupont@gmail.com">
                        </div>
                        <div class="input-group mb-2 mr-sm-2">
                            <div class="input-group-prepend">
                                <div class="input-group-text">Nom</div>
                            </div>
                            <input type="text" 
                                   class="form-control" 
                                   name="name"
                                   value="{{old('name')}}" 
                                   placeholder="Votre nom">
                        </div>
                        <div class="input-group mb-2 mr-sm-2">
                            <div class="input-group-prepend">
                                <div class="input-group-text">Prenom</div>
                            </div>
                            <input type="text" 
                                   class="form-control" 
                                   name="prenom"
                                   value="{{old('prenom')}}" 
                                   placeholder="Votre prenom">
                        </div>
                        <div class="input-group mb-2 mr-sm-2">
                            <div class="input-group-prepend">
                                <div class="input-group-text">Adresse</div>
                            </div>
                            <input type="text" 
                                   class="form-control" 
                                   name="adresse"
                                   value="{{old('adresse')}}" 
                                   placeholder="Votre adresse">
                        </div>
                        <div class="input-group mb-2 mr-sm-2">
                            <div class="input-group-prepend">
                                <div class="input-group-text">Telephone</div>
                            </div>
                            <input type="text" 
                                   class="form-control" 
                                   name="telephone" 
                                   value="{{old('telephone')}}" 
                                   placeholder="Votre numéro de téléphone">
                        </div>
                        <div class="input-group mb-2 mr-sm-2">
                            <div class="input-group-prepend">
                                <div class="input-group-text">Ville</div>
                            </div>
                            <input type="text" 
                                   class="form-control" 
                                   name="ville"
                                   value="{{old('ville')}}" 
                                   placeholder="Votre ville">
                        </div>
                        <div class="input-group mb-2 mr-sm-2">
                            <div class="input-group-prepend">
                                <div class="input-group-text">Age</div>
                            </div>
                            <input type="text" 
                                   class="form-control" 
                                   name="age"
                                   value="{{old('age')}}" 
                                   placeholder="Votre age">
                        </div>
                        <div class="input-group mb-2 mr-sm-2">
                            <div class="input-group-prepend">
                                <div class="input-group-text">Mot de passe</div>
                            </div>
                            <input type="password" 
                                   class="form-control" 
                                   name="password" 
                                   value="" 
                                   placeholder="Mot de passe">
                        </div>
                        <div class="input-group mb-2 mr-sm-2">
                            <div class="input-group-prepend">
                                <div class="input-group-text">Validation du mot de passe</div>
                            </div>
                            <input type="password" 
                                   class="form-control" 
                                   name="passwordValidation" 
                                   value="" 
                                   placeholder="Mot de passe">
                        </div>
                        <div class="input-group mb-2 mr-sm-2">
                            <div class="input-group-prepend">
                                <div class="input-group-text">Quelle fonction occupez vous ?</div>
                            </div>
                            <select name="role"> 
                                <option value="1">Visiteur</option>
                                <option value="2">Secrétaire</option>
                            </select>
                        </div>
                        <button type="submit" name="Envoyer" class="btn btn-primary mb-2">Inscription</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
</div>
</div>
</body>
</html>
@endsection