@extends('layouts.app')

@section('content')
<div class="container">
    @foreach($errors->all() as $error)
    <p style="color: red">{{ $error }}</p>
    @endforeach
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Votre profil') }}</div>         
                <div class="card-body">
                    <ul>
                        <li> Nom : {{$user->name}}</li>
                        <li> Prenom : {{$user->prenom}}</li>
                        <li> Email : {{$user->email}}</li>
                        <li> Age : {{$user->age}}</li>
                        <li> Téléphone : {{$user->telephone}}</li>
                        <li> Ville : {{$user->ville}}</li>
                        <li> Addresse : {{$user->adresse}}</li>
                        <li> Nombre de rendez-vous prévus : {{$nbRdv}}</li>
                        @if(count(Auth::user()->userDroit) == 1)
                        <li> Votre rôle : {{Auth::user()->userDroit[0]->droit->name}}</li>
                        @else
                        <li>Vos rôles :
                            @foreach(Auth::user()->userDroit as $userDroit)
                            {{$userDroit->droit->name}} ;
                            @endforeach
                        </li>
                        @endif                        
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection