@extends('layouts.app')

@section('content')
<div class="container">
    @foreach($errors->all() as $error)
    <p style="color: red">{{ $error }}</p>
    @endforeach
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Rendez-vous du '.$rdv->jour) }}</div>

                <div class="card-body">
                    <ul>
                        <li>Date : {{$rdv->jour}}</li>
                        <li>Heure : {{$rdv->heures}}</li>
                        <li>Motif : {{$rdv->motif}}</li>
                        <li>Medecin : {{$medecin->name . ' ' . $medecin->prenom}}</li>
                        <li>Laboratoire
                            <ul>
                                <li> Addresse : {{$laboratoire->adresse}}</li>
                                <li> Region : {{$laboratoire->region}}</li>
                            </ul>
                    </ul>
                    <form action="{{URL::route('modifRdv', ['date' => $rdv->jour, 'heure' => $rdv->heures])}}" method='POST'>
                        @csrf
                        <input type="hidden" name='medecin' value="{{$medecin->id}}">
                        <input type="hidden" name='date' value="{{$rdv->jour}}">
                        <input type="hidden" name='heure' value="{{$rdv->heures}}">
                        <input class="float-left btn btn-primary" type='submit' name="modify" value='Modifier'>
                    </form>
                    <form action="{{URL::route('suppRdv', ['date' => $rdv->jour, 'heure' => $rdv->heures])}}" method='POST'>
                        @csrf
                        <input type="hidden" name='medecin' value="{{$medecin->id}}">
                        <input type="hidden" name='date' value="{{$rdv->jour}}">
                        <input type="hidden" name='heure' value="{{$rdv->heures}}">
                        <input class="float-right btn btn-danger" onclick="return confirm('Voulez-vous vraiment supprimer le rendez-vous ?')" type='submit' name="delete" value='Supprimer'>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection