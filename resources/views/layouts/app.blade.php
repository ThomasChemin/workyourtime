<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <!-- CSRF Token -->
        <meta name="csrf-token" content="{{ csrf_token() }}">

        <title>WorkYourTime</title>

        <!-- Scripts -->
        <script src="{{ asset('js/app.js') }}" defer></script>
        <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" 
                integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" 
        crossorigin="anonymous"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" 
                integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" 
        crossorigin="anonymous"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" 
                integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" 
        crossorigin="anonymous"></script>

        <!-- Fonts -->
        <link rel="dns-prefetch" href="//fonts.gstatic.com">

        <!-- Styles -->
        <link href="{{ asset('css/app.css') }}" rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">
        <link rel="stylesheet" 
              href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" 
              integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" 
              crossorigin="anonymous">
    </head>
    <body>
        <div id="app">
            <!-- Barre de navigation -->
            <nav class="navbar navbar-expand-lg navbar-light bg-light">
                <a class="navbar-brand" href="{{ URL::route('accueil') }}"><img style="width: 50px" src="/images/Logo-gsb.png">
                    <path fill-rule="evenodd" 
                          d="M0 8a8 8 0 1 1 16 0A8 8 0 0 1 0 8zm7.5-6.923c-.67.204-1.335.82-1.887 1.855A7.97 7.97 0 0 0 5.145 4H7.5V1.077zM4.09 4H2.255a7.025 7.025 0 0 1 3.072-2.472 6.7 6.7 0 0 0-.597.933c-.247.464-.462.98-.64 1.539zm-.582 3.5h-2.49c.062-.89.291-1.733.656-2.5H3.82a13.652 13.652 0 0 0-.312 2.5zM4.847 5H7.5v2.5H4.51A12.5 12.5 0 0 1 4.846 5zM8.5 5v2.5h2.99a12.495 12.495 0 0 0-.337-2.5H8.5zM4.51 8.5H7.5V11H4.847a12.5 12.5 0 0 1-.338-2.5zm3.99 0V11h2.653c.187-.765.306-1.608.338-2.5H8.5zM5.145 12H7.5v2.923c-.67-.204-1.335-.82-1.887-1.855A7.97 7.97 0 0 1 5.145 12zm.182 2.472a6.696 6.696 0 0 1-.597-.933A9.268 9.268 0 0 1 4.09 12H2.255a7.024 7.024 0 0 0 3.072 2.472zM3.82 11H1.674a6.958 6.958 0 0 1-.656-2.5h2.49c.03.877.138 1.718.312 2.5zm6.853 3.472A7.024 7.024 0 0 0 13.745 12H11.91a9.27 9.27 0 0 1-.64 1.539 6.688 6.688 0 0 1-.597.933zM8.5 12h2.355a7.967 7.967 0 0 1-.468 1.068c-.552 1.035-1.218 1.65-1.887 1.855V12zm3.68-1h2.146c.365-.767.594-1.61.656-2.5h-2.49a13.65 13.65 0 0 1-.312 2.5zm2.802-3.5h-2.49A13.65 13.65 0 0 0 12.18 5h2.146c.365.767.594 1.61.656 2.5zM11.27 2.461c.247.464.462.98.64 1.539h1.835a7.024 7.024 0 0 0-3.072-2.472c.218.284.418.598.597.933zM10.855 4H8.5V1.077c.67.204 1.335.82 1.887 1.855.173.324.33.682.468 1.068z"/>
                    </svg> Work Your Time</a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" 
                        data-target="#navbarSupportedContent" 
                        aria-controls="navbarSupportedContent" 
                        aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>

                <div class="collapse navbar-collapse" 
                     id="navbarSupportedContent">
                    <ul class="navbar-nav mr-auto">
                        <li class="nav-item active">
                            <a class="nav-link" 
                               href="{{ URL::route('accueil') }}">Accueil<span 
                                    class="sr-only">(current)</span></a>
                        </li>
                        <!-- Dropdown Connexion -->
                        <li class="nav-item dropdown active">
                            <a class="nav-link dropdown-toggle" href="#" 
                               id="navbarDropdown" role="button" 
                               data-toggle="dropdown" 
                               aria-haspopup="true" 
                               aria-expanded="false">
                                Connexion
                            </a>
                            <div class="dropdown-menu active" 
                                 aria-labelledby="navbarDropdown">
                                <!-- Authentication Links -->
                                @if (Route::has('login'))
                                <a class="dropdown-item" href="{{ route('login') }}">{{ __('Connexion') }}</a>
                                @endif

                                @if (Route::has('register'))
                                <a class="dropdown-item" href="{{ route('inscription') }}">{{ __('Inscription') }}</a>
                                @endif
                                @if(Auth::check())
                                <a class="dropdown-item" 
                                   href="{{ URL::route('profil') }}">Profil</a>
                                <div class="dropdown-divider"></div>
                                <a class="dropdown-item" 
                                   href="{{ URL::route('deconnexion') }}">Deconnexion</a>
                                @endif
                            </div>
                        </li>
                        @if (Auth::check())
                        @foreach(Auth::user()->userDroit as $userDroit)
                        @if($userDroit->idDroit == 3)
                        <a class="nav-link active" 
                           href="{{URL::route('addLaboratory')}}">Ajouter un laboratoire</a>
                        @endif
                        @if($userDroit->idDroit == 1)
                        <a class="nav-link active" 
                           href="{{ URL::route('region') }}">Prendre rendez vous</a>
                        <a class="nav-link active" 
                           href="{{URL::route('calendarVisiteur', ['id' => Auth::id()])}}">Voir ses rendez-vous</a>
                        @endif
                        @if($userDroit->idDroit == 2)
                        @if(Auth::user()->idMedecinRef == null)
                        <a class="nav-link active" 
                           href="{{ URL::route('region') }}">Choisir un médecin référent</a>
                        @else
                        <a class="nav-link active" 
                           href="{{ URL::route('showDispoOfMedecinForSec', ['id' => Auth::user()->idMedecinRef]) }}">Modifier les rendez-vous d'un médecin</a>
                        @endif
                        @endif
                        @endforeach
                        @endif
                        <!-- Fin dropdown -->
                    </ul>
                    @if (Auth::check())
                    <ul class="navbar-nav ml-auto">

                        <li class="nav-item dropdown active">
                            <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                Connecté en tant que {{ Auth::user()->name }}
                            </a>

                            <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                                <a class="dropdown-item" href="{{ route('logout') }}"
                                   onclick="event.preventDefault();
    document.getElementById('logout-form').submit();">
                                    {{ __('Deconnexion') }}
                                </a>

                                <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                                    @csrf
                                </form>
                            </div>
                        </li>
                    </ul>
                    @elseif(old('confirmation') != null)
                    {{ old('confirmation') }}
                    @else
                    Pas connecté
                    @endif
                </div>
            </nav>
            <!-- Fin barre navigation-->

            <main class="py-4">
                @yield('content')
            </main>
        </div>
    </body>
</html>
