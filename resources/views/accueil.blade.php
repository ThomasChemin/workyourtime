@extends('layouts.app')

@section('content')
<div class="container">
    @foreach($errors->all() as $error)
    <p style="color: red">{{ $error }}</p>
    @endforeach
    <h2 class="text-center">
        Bienvenue sur Work Your Time !
    </h2>
    <div class="text-center">
        <p class="h5">
            Vous pouvez prendre des rendez-vous avec des chercheurs de différents laboratoires.
            <br>
            Pour prendre des rendez-vous vous devez vous connecter.
            <br>
            <button type="button" class="btn btn-primary" onclick="location.href ='{{ URL::route('connexion')}}'">Connexion</button> 
            <br>
            <br>
            Si vous n'avez pas de compte, vous pouvez en créer un.
            <br>
            <button type="button" class="btn btn-primary" onclick="location.href ='{{ URL::route('inscription')}}'">Inscription</button>
        </p>
    </div>
</div>
@endsection
