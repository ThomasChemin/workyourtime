@extends('layouts.app')

@section('content')
<div class="container">
    @foreach($errors->all() as $error)
    <p style="color: red">{{ $error }}</p>
    @endforeach
    @if(count($labos)==0)
    <div class="text-center">
        <p> Il n'y a pas de laboratoire dans cette région. </p>
        <a href='{{URL::previous()}}'><button type="button" class="btn btn-primary">Retour</button></a>
    </div>
    @else

    <table class="table table-striped">
        <thead class="thead-dark">
            <tr>
                <th>#</th>
                <th>Adresse</th>
                <th>Check</th>
            </tr>
        </thead>
        <tbody>
            @foreach($labos as $labo)
            <tr>
                <th scope="row">{{$loop->index+1}}</th>
                <td>
                    {{$labo->adresse}}
                </td>
                <td>
                    <a href = "{{URL::route('showMedFromLabo', ['id' => $labo->id])}}">
                        <button type="button" class="btn btn-primary">
                            Choisir ce laboratoire
                        </button>
                    </a>
                </td>
            </tr>
            @endforeach
        </tbody>
    </table>
    <div class="pagination" style='display: flex;
         justify-content: center;'>
        {{ $labos->links() }}
    </div>
    @endif
</div>

@endsection