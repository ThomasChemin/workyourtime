@extends('layouts.app')

@section('content')
<script src="https://code.jquery.com/jquery-3.5.1.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/moment.js/2.9.0/moment.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/fullcalendar@5.5.0/main.min.js"></script>
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/fullcalendar@5.5.0/main.css"/>
<div class="container">
    @foreach($errors->all() as $error)
    <p style="color: red">{{ $error }}</p>
    @endforeach
    @isset($medecin)
    <h2> Agenda de {{$medecin->name . ' ' . $medecin->prenom}} </h2>
    @endif
    @if($personalCalendar)
    <a href="{{URL::route('suppAllRdvPass')}}" class="btn btn-danger" onclick="return confirm('Voulez-vous vraiment supprimer les rendez-vous ?')">
        Supprimer tous les rendez-vous passés
    </a>
    @endif
    <div id='calendar-{{$calendar->calendar()}}'>
        {{ $calendar->script() }}
    </div>
    <form>

    </form>
</div>
@endsection