@extends('layouts.app')

@section('content')

<div class="container">
    @foreach($errors->all() as $error)
    <p style="color: red">{{ $error }}</p>
    @endforeach
    @if(count($medecins)==0)
    <div class="text-center">
        <p class="text-center"> Il n'y a pas de médecins dans ce laboratoire. </p>
        <a href='{{URL::previous()}}'><button type="button" class="btn btn-primary">Retour</button></a>
    </div>
    @else

    <table class="table table-striped">
        <thead class="thead-dark">
            <tr>
                <th>#</th>
                <th>Nom</th>
                <th>Prenom</th>
                <th>Email</th>
                <th>Check</th>
            </tr>
        </thead>
        <tbody>
            @foreach($medecins as $medecin)
            <tr>
                <th scope="row">{{$loop->index+1}}</th>
                <td>
                    {{$medecin->name}}
                </td>
                <td>
                    {{$medecin->prenom}}
                </td>
                <td>
                    {{$medecin->email}}
                </td>
                <td>
                    @foreach(Auth::user()->userDroit as $userDroit)
                    @if($userDroit->droit->id == 1)
                    <a href = "{{URL::route('showDispoOfMedecin', ['id' => $medecin->id])}}">
                        <button type="button" class="btn btn-primary">
                            Choisir ce medecin
                        </button>
                    </a>
                    @elseif($userDroit->droit->id == 2 && Auth::user()->idMedecinRef == null)
                    <a href = "{{URL::route('attributeMedtoSec', ['idMed' => $medecin->id])}}">
                        <button type="button" class="btn btn-primary">
                            Choisir ce medecin
                        </button>
                    </a>
                    @endif
                    @endforeach
                </td>
            </tr>
            @endforeach
        </tbody>
    </table>
    <div class="pagination" style='display: flex;
         justify-content: center;'>
        {{ $medecins->links() }}
    </div>
    @endif
</div>
@endsection