@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Rendez-vous du '.$rdv->jour) }}</div>
                @foreach($errors->all() as $error)
                <p style="color: red">{{ $error }}</p>
                @endforeach
                <div class="card-body">
                    <form method="POST" action="{{URL::route('doModifRdv')}}">
                        @csrf
                        <ul>
                            <input type='hidden' name="last_date" value="{{$rdv->jour}}">
                            <input type='hidden' name="last_heure" value="{{$rdv->heures}}">
                            <input type='hidden' name="last_motif" value="{{$rdv->motif}}">
                            <input type='hidden' name="last_medecin" value="{{$rdv->medecin->id}}">
                            <li>Date : <input class='float-right' type="date" name="date" value="{{$rdv->jour}}"></li><br>
                            <li>Heure : <input class='float-right' type="text" name="heure" value='{{$rdv->heures}}'></li><br>
                            <li>Motif : <input class='float-right' type="text" name="motif" value='{{$rdv->motif}}'></li><br>
                            <li>Medecin : 
                                <select class='float-right' name="medecin">
                                    @foreach($rdv->medecin->laboratory->medecin as $medecin)
                                    <option @if($rdv->medecin->email == $medecin->email)selected @endif value="{{$medecin->id}}">{{$medecin->name . ' ' .$medecin->prenom}}</option>
                                    @endforeach
                                </select>
                            </li>
                            <li>Laboratoire
                                <ul>
                                    <li> Addresse : {{$rdv->medecin->laboratory->adresse}}</li>
                                    <li> Region : {{$rdv->medecin->laboratory->region}}</li>
                                </ul>
                        </ul>
                        <input class="float-left btn btn-primary" type='submit' value='Modifier'>
                    </form>
                    <a class="float-right btn btn-danger" href='{{ URL::route('calendarVisiteur', ['id' => Auth::id()]) }}'>
                        Annuler
                    </a>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection