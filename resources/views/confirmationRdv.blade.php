@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Rendez-vous pour le '.$date." à ".$heure."h") }}</div>

                <div class="card-body">
                    @foreach($errors->all() as $error)
                    <p style="color: red">{{ $error }}</p>
                    @endforeach
                    <ul>
                        <li>Date : {{$date}}</li>
                        <li>Heure : {{$heure}}</li>
                        <li>Medecin : {{$medecin->name . ' ' . $medecin->prenom}}</li>
                        <li>Laboratoire
                            <ul>
                                <li> Addresse : {{$labo->adresse}}</li>
                                <li> Region : {{$labo->region}}</li>
                            </ul>
                            <form action="{{URL::route('validateRdv')}}" method='POST'>
                                @csrf
                                <li>Motif <input type='text' name='motif' placeholder="Entrer votre motif de visite"></li><br>
                                <input type='hidden' name="medecin" value='{{$medecin->id}}'>
                                <input type='hidden' name="heure" value='{{$heure}}'>
                                <input type='hidden' name="date" value='{{$date}}'>
                                <input class="float-left btn btn-primary" type="submit" name='confirm' value="Confirmer">
                                <a class="float-right btn btn-danger" href="{{URL::route('showDispoOfMedecin', ['id' => $medecin->id])}}">
                                    Annuler
                                </a>
                            </form>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection