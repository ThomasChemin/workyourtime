@extends('layouts.app')

@section('content')
<div class="container">
    @foreach($errors->all() as $error)
    <p style="color: red">{{ $error }}</p>
    @endforeach
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Ajouter un laboratoire') }}</div>

                <div class="card-body">
                    @if (session('status'))
                    <div class="alert alert-success" role="alert">
                        {{ session('status') }}
                    </div>
                    @endif
                    @foreach($errors->all() as $error)
                    <p style="color: red">{{ $error }}</p>
                    @endforeach
                    @if(old('successInsert'))
                    <script>
                        var success = (function succes() {
                            alert("{{old('successInsert')}}");
                        }());
                    </script>
                    @endif
                    <div>
                        <form method="POST" action="{{URL::route('doAddLaboratory')}}">
                            @csrf
                            <div class="form-group row">
                                <label class="col-sm-2 col-form-label">Région : </label>
                                <select class="form-control" name="region">
                                    <option value="Auvergne Rhone Alpes">Auvergne-Rhône-Alpes</option>
                                    <option value="Bourgogne Franche Comté">Bourgogne-Franche-Comté</option>
                                    <option value="Bretagne">Bretagne</option>
                                    <option value="Centre Val de Loire">Centre-Val de Loire</option>
                                    <option value="Corse">Corse</option>
                                    <option value="Grand Est">Grand Est</option>
                                    <option value="Hauts de France">Hauts-de-France</option>
                                    <option value="Ile de France">Île-de-France</option>
                                    <option value="Normandie">Normandie</option>
                                    <option value="Nouvelle Aquitaine">Nouvelle-Aquitaine</option>
                                    <option value="Occitanie">Occitanie</option>
                                    <option value="Pays de la Loire">Pays de la Loire</option>
                                    <option value="Provence Alpes Cote d'Azur">Provence-Alpes-Côte d'Azur</option>
                                    <option value="Guadeloupe">Guadeloupe</option>
                                    <option value="Martinique">Martinique</option>
                                    <option value="Guyane">Guyane</option>
                                    <option value="La Réunion">La Réunion</option>
                                    <option value="Mayotte">Mayotte</option>
                                </select>
                                <label class="col-sm-2 col-form-label">Adresse : </label>
                                <input class="form-control" type="text" name="adresse" value='{{old('adresse')}}'>
                            </div>
                            <div class="text-center">
                                <input class="btn btn-primary" type="submit" value="Ajouter">
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
