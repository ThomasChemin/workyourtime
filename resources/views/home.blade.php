@extends('layouts.app')

@section('content')
<div class="container">
    @foreach($errors->all() as $error)
    <p style="color: red">{{ $error }}</p>
    @endforeach
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Tableau de bord') }}</div>

                <div class="card-body">
                    @if (session('status'))
                    <div class="alert alert-success" role="alert">
                        {{ session('status') }}
                    </div>
                    @endif

                    {{ __('Vous êtes déjà connecté !') }}
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
