## Before launching the WEB APPLICATION

Go the your folder 'WorkYourTime'.
Create a file .env following the example of the .env.example
Replace some lines with your own settings.
You have to change : 
- DB_HOST= The IP of your database
- DB_DATABASE= yourDatabase
- DB_USERNAME= yourUsername
- DB_PASSWORD= thePasswordOfYourUser

Then, launch a terminal un the project and type.

<code>./artisan key:generate</code>

It will replace your APP_KEY

## Creating the database and inserting data

Go the your folder 'WorkYourTime', launch a terminal in the project and type.

<code>./artisan migrate:fresh
./artisan db:seed
</code>

It will create the database and insert some data in it.

## Launch your project

To launch the project, open a terminal in your project 'WorkYourTime' and type :

<code>./artisan serve</code>
