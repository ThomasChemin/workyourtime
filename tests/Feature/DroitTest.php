<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;
use App\Models\Droit;

class DroitTest extends TestCase
{
    
    public function setUp(): void {
        parent::setUp();
        \Illuminate\Support\Facades\Artisan::call('migrate:fresh');
        \Illuminate\Support\Facades\Artisan::call('db:seed');
    }
    
    /**
     * Test the different Droit
     *
     * @return void
     */
    public function testDroit()
    {
        $this->assertEquals('Visiteur', (Droit::find(1))->name, 'Le droit 1 ne correspond pas au droit Visiteur');
        $this->assertEquals('Secretaire', (Droit::find(2))->name, 'Le droit 2 ne correspond pas au droit Secretaire');
        $this->assertEquals('Admin', (Droit::find(3))->name, 'Le droit 3 ne correspond pas au droit Admin');
    }
}
