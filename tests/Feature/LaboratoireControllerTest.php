<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class LaboratoireControllerTest extends TestCase
{
    public function setUp(): void {
        parent::setUp();
        \Illuminate\Support\Facades\Artisan::call('migrate:fresh');
        \Illuminate\Support\Facades\Artisan::call('db:seed');
    }
    
    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function testShowLaboratoriesFromRegion()
    {
        $user = \App\Models\User::find(1);
        $response = $this->actingAs($user)->get('/rdv/region');
        $response->assertStatus(404);
        $response = $this->actingAs($user)->get('/rdv/region/Grand Est');
        $response->assertStatus(200);
    }
    
    public function testAddLaboratoryForAdmin(){
        $user = \App\Models\User::find(1);
        $response = $this->actingAs($user)->get('/admin/addLaboratory');
        $response->assertStatus(200);
        $userTwo = \App\Models\User::find(2);
        $response = $this->actingAs($userTwo)->get('/admin/addLaboratory');
        $response->assertStatus(302);
    }
}
