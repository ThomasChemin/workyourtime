<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class HomeControllerTest extends TestCase
{
    public function setUp(): void {
        parent::setUp();
        \Illuminate\Support\Facades\Artisan::call('migrate:fresh');
        \Illuminate\Support\Facades\Artisan::call('db:seed');
    }
    
    /**
     * Test if a user logged can access to / or /home.
     *
     * @return void
     */
    public function testHome() {
        $user = \App\Models\User::find(1);
        $response = $this->actingAs($user)->get('/');
        $response->assertStatus(200);
        $response = $this->actingAs($user)->get('/home');
        $response->assertStatus(200);
    }
}
