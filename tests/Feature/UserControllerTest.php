<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class UserControllerTest extends TestCase
{
    public function setUp(): void {
        parent::setUp();
        \Illuminate\Support\Facades\Artisan::call('migrate:fresh');
        \Illuminate\Support\Facades\Artisan::call('db:seed');
    }
    
    /**
     * Test if a user can access the page Inscription
     *
     * @return void
     */
    public function testShowInscription()
    {
        $response = $this->get('/utilisateur/inscription');
        $response->assertStatus(200);
    }
    
    /**
     * Test if a user logged can access the page Connexion
     */
    public function testShowProfil(){
        $response = $this->get('/utilisateur/profil');
        $response->assertStatus(302);
        $user = \App\Models\User::find(1);
        $response = $this->actingAs($user)->get('/utilisateur/profil');
        $response->assertStatus(200);
    }
}
