<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class LoginControllerTest extends TestCase
{
    public function setUp(): void {
        parent::setUp();
        \Illuminate\Support\Facades\Artisan::call('migrate:fresh');
        \Illuminate\Support\Facades\Artisan::call('db:seed');
    }
    
    /**
     * Test if a user can access to the page Login
     *
     * @return void
     */
    public function testShowLogin()
    {
        $response = $this->get('/utilisateur/connexion');
        $response->assertStatus(200);
    }
}
