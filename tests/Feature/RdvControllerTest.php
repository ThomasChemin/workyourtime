<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class RdvControllerTest extends TestCase {

    public function setUp(): void {
        parent::setUp();
        \Illuminate\Support\Facades\Artisan::call('migrate:fresh');
        \Illuminate\Support\Facades\Artisan::call('db:seed');
        $this->user = \App\Models\User::find(1);
        $this->rdv = \App\Models\Rdv::create([
                    'jour' => '2021-02-22',
                    "heures" => 10,
                    "motif" => "TestGererRdvMed",
                    "idUser" => $this->user->id,
                    "idMedecin" => 1
        ]);
    }

    /**
     * Test if a user can access the page GererRdvMed
     *
     * @return void
     */
    public function testGererRdvMed() {
        $response = $this->get('/rdvSec/gererRdvMed');
        $response->assertStatus(404);
        $response = $this->actingAs($this->user)->get('/rdvSec/gererRdvMed');
        $response->assertStatus(404);
        $response = $this->actingAs($this->user)->get("/rdvSec/gererRdvMed/" . $this->rdv->idMedecin . "/" . $this->rdv->jour . "/" . $this->rdv->heures);
        $response->assertStatus(200);
    }

    /**
     * Test if a user can access the page Confirmation
     *
     * @return void
     */
    public function testConfirmation() {
        $response = $this->get('/rdvVis/confirmation');
        $response->assertStatus(404);
        $response = $this->actingAs($this->user)->get('/rdvVis/confirmation');
        $response->assertStatus(404);
        $response = $this->actingAs($this->user)->get("/rdvVis/confirmation/" . $this->rdv->idMedecin . "/" . $this->rdv->jour . "/" . $this->rdv->heures);
        $response->assertStatus(200);
    }

    /**
     * Test if a user can access the page modifRdv
     */
    public function testModifRdv() {
        $response = $this->get('/rdv/modifRdv');
        $response->assertStatus(404);
        $response = $this->actingAs($this->user)->get('/rdv/modifRdv');
        $response->assertStatus(404);
        $response = $this->actingAs($this->user)->get("/rdv/modifRdv/2021-02-22/10");
        $response->assertStatus(200);
    }

}
