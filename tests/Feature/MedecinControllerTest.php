<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class MedecinControllerTest extends TestCase {

    public function setUp(): void {
        parent::setUp();
        \Illuminate\Support\Facades\Artisan::call('migrate:fresh');
        \Illuminate\Support\Facades\Artisan::call('db:seed');
    }

    /**
     * Test if a user can acces the page showMedFromLabo
     *
     * @return void
     */
    public function testShowMedecinFromLaboratory() {
        $response = $this->get('/rdv/showMedFromLabo');
        $response->assertStatus(302);
        $user = \App\Models\User::find(1);
        $response = $this->actingAs($user)->get('/rdv/showMedFromLabo');
        $response->assertStatus(302);
        $response = $this->actingAs($user)->get('/rdv/showMedFromLabo/1');
        $response->assertStatus(200);
    }

    /**
     * Test if a user can access the page showDispoOfMedecinSec
     */
    public function testShowDispoOfMedecinSec() {
        $response = $this->get('/rdvSec/showDispoOfMedecinSec');
        $response->assertStatus(302);
        $user = \App\Models\User::find(1);
        $response = $this->actingAs($user)->get('/rdvSec/showDispoOfMedecinSec');
        $response->assertStatus(302);
        $response = $this->actingAs($user)->get('/rdvSec/showDispoOfMedecinSec/1');
        $response->assertStatus(200);
    }

    /**
     * Test if a user can access the page showDispoOfMedecin
     */
    public function testShowDispoOfMedecin() {
        $response = $this->get('/rdvVis/showDispoOfMedecin');
        $response->assertStatus(302);
        $user = \App\Models\User::find(1);
        $response = $this->actingAs($user)->get('/rdvVis/showDispoOfMedecin');
        $response->assertStatus(302);
        $response = $this->actingAs($user)->get('/rdvVis/showDispoOfMedecin/1');
        $response->assertStatus(200);
    }

}
